package Liczby;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int liczbaElementow;
        do {
            System.out.println("podaj liczbe elementow zbioru");
            liczbaElementow = scanner.nextInt();
        } while (liczbaElementow <= 0);

        List<Integer> listaLiczb = new ArrayList<>();

        System.out.println("podaj liczbe: ");

        int liczbaDoDodania = scanner.nextInt();
        while (liczbaDoDodania < 0) {
            listaLiczb.add(liczbaDoDodania);
        }
        listaLiczb.add(liczbaDoDodania);

        for (int index = 1; index < liczbaElementow; ) {
            System.out.println("podaj liczbę " + (index + 1) + ": ");
            liczbaDoDodania = scanner.nextInt();


            if (liczbaDoDodania >= listaLiczb.get(index - 1)) {
                listaLiczb.add(liczbaDoDodania);
                index++;
            }
        }

        System.out.println("podaj liczbe dodatkowa");
        int liczbaDodatkowa = scanner.nextInt();
        boolean znalezionoLiczbe = false;


 //       for (int i = 0; i < listaLiczb.size() - 1 && !znalezionoLiczbe; i++) {
 //           for (int j = i + 1; j < listaLiczb.size(); j++) {
//
 //               if (listaLiczb.get(i) + listaLiczb.get(j) == liczbaDodatkowa) {
 //                   System.out.println("znaleziona para");
//
 //                   znalezionoLiczbe = true;
 //                   break;
 //               }
 //           }
 //       }
 //       if (!znalezionoLiczbe) {
 //           System.out.println("nieznaleziona para ");
 //       }

        znalezionoLiczbe = false;
        int i = 0, j = listaLiczb.size() - 1;

        while(i < j) {
            int suma = listaLiczb.get(i) + listaLiczb.get(j);
            if (suma == liczbaDodatkowa) {
                System.out.println("znaleziona para ");
                znalezionoLiczbe = true;
                break;
            } else if (suma > liczbaDodatkowa) {
                j--;
            } else {
                i++;
            }
        }
        if (!znalezionoLiczbe){
            System.out.println("nieznaleziono pary");
        }
    }
}