package Zwierzaki.jedzenie;

public enum TypJedzenia {
    MIESO("ziarno"),
    MLEKO("mleko"),
    KARMA("karma");

    String mojaNazwa;

    TypJedzenia(String nazwa){
        mojaNazwa = nazwa;
    }

    TypJedzenia(){
        System.out.println(name());
    }

    @Override
    public String toString(){
        return mojaNazwa;
    }
}
