package Zwierzaki.jedzenie;

import java.util.ArrayList;
import java.util.List;

public class PojemnikNaJedzenie {

    List<Jedzenie> jedzenie = new ArrayList<>();

    public void dodajJedzenie(Jedzenie jedzenieDoDodania) {

        int indexJedzenia = jedzenie.indexOf(jedzenieDoDodania);
        if (indexJedzenia >= 0){

            Jedzenie jedzenieWLodowce = jedzenie.get(indexJedzenia);
            jedzenieWLodowce.dodajJedzenie(jedzenieDoDodania);

        }else {
            jedzenie.add(jedzenieDoDodania);
        }
    }
}
