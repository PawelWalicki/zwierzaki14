package Zwierzaki.jedzenie;

public class Jedzenie {
    TypJedzenia typJedzenia;
    int  ilosc;

    public Jedzenie(TypJedzenia typJedzenia, int ilosc) {
        this.typJedzenia = typJedzenia;
        this.ilosc = ilosc;
    }

    @Override
    public String toString(){
        return typJedzenia.toString() + ": " + ilosc;
    }
    public void dodajJedzenie(Jedzenie jedzenieDoDodania){
        ilosc += jedzenieDoDodania.ilosc;
    }
}
