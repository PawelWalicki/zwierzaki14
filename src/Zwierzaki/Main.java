package Zwierzaki;

import Zwierzaki.jedzenie.Jedzenie;
import Zwierzaki.jedzenie.TypJedzenia;
import Zwierzaki.zwierz.Kot;
import Zwierzaki.zwierz.Pies;

public class Main {

    public static void main(String[] args) {

        Osoba Pawel = new Osoba("Pawel");
        Pies burek = new Pies("burek");
        Kot rudy = new Kot ("rudy");

        Pawel.dodajZwierzaka(burek);
        Pawel.dodajZwierzaka(rudy);

        Jedzenie miesoDolodowki = new Jedzenie(TypJedzenia.MIESO,4);
        Jedzenie mlekoDolodowki = new Jedzenie(TypJedzenia.MLEKO,4);
        Jedzenie karmaDolodowki = new Jedzenie(TypJedzenia.KARMA,4);

        Pawel.dodajDoLodowki(miesoDolodowki);
        Pawel.dodajDoLodowki(mlekoDolodowki);
        Pawel.dodajDoLodowki(karmaDolodowki);


        Jedzenie miesoDlaPsa = new Jedzenie(TypJedzenia.MIESO,3);
        burek.dodajGlod(miesoDlaPsa);

        Jedzenie mlekoDlaKota = new Jedzenie(TypJedzenia.MLEKO,2);
        rudy.dodajGlod(mlekoDlaKota);


        System.out.println(Pawel.przedstawSie());
        System.out.println(burek.dajGlos());


    }
}
