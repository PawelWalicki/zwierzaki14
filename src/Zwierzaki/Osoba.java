package Zwierzaki;

import Zwierzaki.jedzenie.Jedzenie;
import Zwierzaki.jedzenie.PojemnikNaJedzenie;
import Zwierzaki.zwierz.Zwierz;
import java.util.ArrayList;
import java.util.List;

public class Osoba {
    String imie;
    List<Zwierz> mojeZwierzaki = new ArrayList<>(); //  dodaje tablice odrazu po starcie apliakcji
    PojemnikNaJedzenie lodowka = new PojemnikNaJedzenie();
    List<Jedzenie> listaZakupow = new ArrayList<>();

    public Osoba (String imie){
        this.imie = imie;
        // mojeZwierzaki = new ArrayList<>(); stowrzy się razem z utworzeniem osoby nie warze czy będzie miała
        //                                    zwierzaka  czy nie chpdzi o opytmalizaje pamięci
    }

    public void zrobZakupy(){

    }
    public void nakarmZwierzaki(){

    }

    public String przedstawSie() {
        return toString();
    }

    @Override
    public String toString() {
        return "mam na imie: " + imie + ".\nMam: " + mojeZwierzaki.size() + " zwierzakow. \n" + "sa to: " +
                mojeZwierzaki.toString() + ".\nw  lodowce mam: " + lodowka.toString() + ".\npowinienem kupic: " +
                listaZakupow.toString();
    }

    public void dodajZwierzaka(Zwierz zwierz){
      //  if (mojeZwierzaki == null){
        //     mojeZwierzaki = new ArrayList<>();
     //   }
        // tutaj zostaje utworzona tablica tylko wtedy kiedy ktoś doda dopiero jakiegoś zwierzaka, inaczej tablica nie zostanie utworzona
        mojeZwierzaki.add(zwierz);
    }


    public void dodajDoListyZakupow(Jedzenie jedzenieDoDodania) {

        int indexJedzenia = listaZakupow.indexOf(jedzenieDoDodania);
        if (indexJedzenia >= 0){

            Jedzenie jedzenieWLodowce = listaZakupow.get(indexJedzenia);
            jedzenieWLodowce.dodajJedzenie(jedzenieDoDodania);

        }else {
            listaZakupow.add(jedzenieDoDodania);
        }

    }


}
