package Zwierzaki.zwierz;

public class Kot extends Zwierz {

    public Kot (String imie) {
        super(imie);
    }

    @Override
    protected String rodzajZwierzaka() {
        return "kot";
    }

    @Override
    public String dajGlos() {
        return "mial, mial";
    }
}
