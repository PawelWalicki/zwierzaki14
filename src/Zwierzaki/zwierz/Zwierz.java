package Zwierzaki.zwierz;

import Zwierzaki.jedzenie.Jedzenie;

import java.util.ArrayList;
import java.util.List;

public abstract class Zwierz {

    String imie;
    List<Jedzenie> glod  = new ArrayList<>();

    public Zwierz(String imie) {
        super();
        this.imie = imie;
    }
    protected abstract String rodzajZwierzaka();

    String przedstawSie(){
        return rodzajZwierzaka() + " " + imie+ ", chce zjesc: " + glod.toString();
    }
    public abstract String dajGlos();

    @Override
    public String toString(){
        return przedstawSie();
    }

    public void dodajGlod (Jedzenie jedzenieDoDodania){
        glod.dodajJedzenie(jedzenieDoDodania);
    }

}
