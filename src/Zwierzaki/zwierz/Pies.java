package Zwierzaki.zwierz;

public class Pies  extends Zwierz {

    public Pies (String imie) {
        super(imie);
    }

    @Override
    protected String rodzajZwierzaka() {
        return "pies";
    }

    @Override
    public String dajGlos() {
        return "hau, hau";
    }


}
